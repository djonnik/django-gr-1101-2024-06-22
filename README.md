# django_gr1101_2024_05_30

---

[video 1](https://youtu.be/58N1YS0-Nss) - intro, Django project, django app, structure, settings

[video 2](https://youtu.be/lBUDOGei7PA) - models, migrations, fields

[video 3](https://youtu.be/tQzQwnFHVuA) - models, backwards relationships, admin/superuser, .save(), .objects()

[video 4](https://youtu.be/Qqd4uAgRyV4) - services, lookups, .filter(), .get(), .all()

[video 5](https://youtu.be/K6ah_rLX72I) - views, urls, templates, context

[video 6](https://youtu.be/7vWl307ksYA) - views, urls, templates, generic.ListView, DDT
