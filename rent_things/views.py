from django.http import HttpResponse, HttpRequest
from django.shortcuts import render
from django.views import generic

from .models import Thing, Category, Tag


def index(request: HttpRequest) -> HttpResponse:
    num_things = Thing.objects.count()
    num_categories = Category.objects.count()
    num_tags = Tag.objects.count()
    context = {
        "num_things": num_things,
        "num_categories": num_categories,
        "num_tags": num_tags,
    }
    return render(request, "rent_things/index.html", context=context)


class ThingListView(generic.ListView):
    model = Thing
    template_name = (
        "rent_things/things_list.html"  # Default: <app_label>/<model_name>_list.html
    )
    context_object_name = "things"  # Default: object_list
    queryset = Thing.objects.filter(is_active=True).select_related(
        "category"
    )  # Default: Model.objects.all()


class CategoryListView(generic.ListView):
    model = Category
    context_object_name = "categories"
