import warnings

from django.db import transaction
from django.db.models import QuerySet

from rent_things.models import Thing


def retrieve(
    id_: int = None,
    /,
    name: str = None,
    description: str = None,
    is_active: bool = True,
    price_from: float = None,
    price_to: float = None,
    category_id: int = None,
    tags_ids: list[int] = None,
    owner_id: int = None,
    compensation: float = None,
) -> Thing | QuerySet:
    if id_:
        if (
            name
            or description
            or is_active
            or price_from
            or price_to
            or category_id
            or tags_ids
            or owner_id
            or compensation
        ):
            warnings.warn(
                f"{retrieve.__name__} function when passing argument to id_, other parameters are ignored"
            )
        return Thing.objects.get(pk=id_)

    queryset = Thing.objects.filter(is_active=is_active)
    if name:
        queryset = queryset.filter(name__iexact=name)
    if description:
        queryset = queryset.filter(description__icontains=description)
    if price_from:
        queryset = queryset.filter(price__gte=price_from)
    if price_to:
        queryset = queryset.filter(price__lte=price_to)
    if category_id:
        queryset = queryset.filter(category_id=category_id)
    if tags_ids:
        queryset = queryset.filter(tags__id__in=tags_ids)
    if owner_id:
        queryset = queryset.filter(owner_id=owner_id)
    if compensation:
        queryset = queryset.filter(compensation=compensation)
    return queryset


def create(
    name: str,
    category_id: int,
    tags_ids: list[int],
    owner_id: int,
    price: float,
    compensation: float,
    description: str = None,
) -> Thing:
    with transaction.atomic():
        thing = Thing.objects.create(
            name=name,
            description=description,
            category_id=category_id,
            price=price,
            compensation=compensation,
            owner_id=owner_id,
        )
        thing.tags.set(tags_ids)
        return thing
