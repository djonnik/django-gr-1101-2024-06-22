from django.urls import path
from rent_things.views import index, ThingListView, CategoryListView


app_name = "rent_app"

urlpatterns = [
    path("", index, name="index"),
    path("things/", ThingListView.as_view(), name="things_list"),
    path("categories/", CategoryListView.as_view(), name="category_list"),
]
